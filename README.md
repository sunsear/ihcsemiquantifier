# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository is for an ImageJ plugin that does a Semi Quantification for 
ImmunoHistoChemistry images
* First Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install Maven
* Install Git
* Clone this project:
    * git clone git@bitbucket.org:sunsear/ihcsemiquantifier.git
* Build through maven:
    * mvn clean install
* Install into FIJI:
    * In Fiji click Install Plugin.
    * Navigate to the target folder in your project
    * select ihssemiquantifier-imagej-plugin-0.0.1-SNAPSHOT.jar
    * Restart FIJI
* Find the menu item in "Plugins" -> "Process" -> "Hello, world"

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact