import java.io.IOException;

import org.scijava.ItemIO;
import org.scijava.command.Command;
import org.scijava.log.LogService;
import org.scijava.plugin.Parameter;
import org.scijava.plugin.Plugin;

import io.scif.services.DatasetIOService;
import net.imagej.Dataset;

@Plugin(type = Command.class, headless = true, menuPath = "Histo>Open test image")
public class TestImageOpenerPlugin implements Command {
    @Parameter
    private DatasetIOService datasetIOService;

    @Parameter(type = ItemIO.OUTPUT)
    private Dataset dataset;

    @Parameter
    private LogService log;

    /**
     * Produces an output with the well-known "Hello, World!" message. The
     * {@code run()} method of every {@link Command} is the entry point for
     * ImageJ: this is what will be called when the user clicks the menu entry,
     * after the inputs are populated.
     */
    @Override
    public void run() {
        try {
            dataset = datasetIOService.open("src/test/resources/CD163 6.3p.tif");
        } catch (IOException e) {
            log.error("Exception occurred during opening of test image.", e);
        }
    }
}
