import java.util.List;

import org.scijava.ItemIO;
import org.scijava.command.Command;
import org.scijava.log.LogService;
import org.scijava.plugin.Parameter;
import org.scijava.plugin.Plugin;

import net.imagej.Dataset;
import net.imagej.ImageJ;
import net.imagej.display.ImageDisplay;
import net.imglib2.Cursor;
import net.imglib2.histogram.Histogram1d;
import net.imglib2.type.numeric.integer.LongType;
import net.imglib2.type.numeric.integer.UnsignedByteType;

@Plugin(type = Command.class, headless = true, menuPath = "Histo>Show Histo")
public class IHCSemiQuantifierPlugin implements Command {

    @Parameter(type = ItemIO.OUTPUT)
    private String greeting;

    @Parameter(type = ItemIO.INPUT)
    private ImageJ ij;

    @Parameter(type = ItemIO.INPUT)
    private Dataset dataset;

    @Parameter(type = ItemIO.INPUT)
    private LogService log;

    /**
     * Produces an output with the well-known "Hello, World!" message. The
     * {@code run()} method of every {@link Command} is the entry point for
     * ImageJ: this is what will be called when the user clicks the menu entry,
     * after the inputs are populated.
     */
    @Override
    public void run() {
        List<ImageDisplay> imageDisplays = ij.imageDisplay().getImageDisplays();
        log.info("Currently have " + imageDisplays.size() + " image windows open");
        Histogram1d<UnsignedByteType> result = (Histogram1d<UnsignedByteType>) ij.op().run(
                net.imagej.ops.Ops.Image.Histogram.class, dataset.getImgPlus());
        log.info("Found histogram with " + result.getBinCount() + " bins.");
        Cursor<LongType> cursor = result.cursor();
        String histo = "";
        int iter = 0;
        long binCount = result.getBinCount();
        while (cursor.hasNext()) {
            cursor.fwd();
            histo += "Bin " + iter + ":" + cursor.get() + "\n";
            iter++;
        }
        greeting = "The histogram for the image you have open has " + binCount + " bins:\n\n" +
                histo;
    }

    /**
     * A {@code main()} method for testing.
     * <p>
     * When developing a plugin in an Integrated Development Environment (such as
     * Eclipse or NetBeans), it is most convenient to provide a simple
     * {@code main()} method that creates an ImageJ context and calls the plugin.
     * </p>
     * <p>
     * In particular, this comes in handy when one needs to debug the plugin:
     * after setting one or more breakpoints and populating the inputs (e.g. by
     * calling something like
     * {@code ij.command().run(MyPlugin.class, "inputImage", myImage)} where
     * {@code inputImage} is the name of the field specifying the input) debugging
     * becomes a breeze.
     * </p>
     *
     * @param args unused
     */
    public static void main(final String... args) {
        // Launch ImageJ as usual.
        final ImageJ ij = new ImageJ();
        ij.launch(args);

        // Open our test image right away
        ij.command().run(TestImageOpenerPlugin.class, true);
    }
}
